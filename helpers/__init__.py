from os.path import abspath, isfile


def is_csv_file(filename):
    """
        Проверка, существует ли файл и имеет ли он расширение .csv

        Parameters
        ----------
        filename: str - путь до предполагаемого файла
    """

    absolute_path = abspath(filename)

    return filename.endswith(".csv") if isfile(absolute_path) else False
