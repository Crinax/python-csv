import csv
from functools import reduce

from domain.house import House


def from_user_group_to_house(grouped_users):
    """
        Маппер из сгруппированных пользователей в дома
        с расчётом суммы начисления

        Parameters
        ----------
        grouped_users: Dict[str, [User]] - сгруппированные пользователи
    """

    result = []

    for index, (house_info, users) in enumerate(
        grouped_users.items(),
        start=1
    ):
        accrual = reduce(
            lambda prev, curr: prev + curr,
            map(lambda user: user.accrual, users)
        )

        result.append(House(
            __index=index,
            number=house_info[1],
            street=house_info[0],
            accrual=accrual,
        ))

    return result


def from_house_to_dict(house):
    """
        Маппер из дома в словарь для

        Parameters
        ----------
        house: House - объект дома
    """

    return {
        "№ строки": house.index,
        "Улица": house.street,
        "№ дома": house.number,
        "Начисленно": str(house.accrual)
    }


def from_house_to_csv(houses, filename):
    """
        Маппер из списка домов в csv файл

        Parameters
        ----------
        house: [House] - список домов
        filename: str - путь до файла, в который нужно сохранить
    """

    with open(filename, "w", encoding="utf-8-sig") as csv_file:
        houses_list = list(map(from_house_to_dict, houses))

        if not houses_list:
            return

        headers = list(houses_list[0].keys())
        csv_writer = csv.DictWriter(
            csv_file,
            fieldnames=headers,
            delimiter=";"
        )

        csv_writer.writeheader()
        csv_writer.writerows(houses_list)
