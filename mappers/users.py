import csv

from domain.users import User
from helpers import is_csv_file


def from_dict_to_user(user_dict):
    """
        Маппер из словаря в объект абонента

        Parametrs
        ---------
        user_dict: Dict - словарь с данными абонента
    """

    return User(
        __index=user_dict["№ строки"],
        surname=user_dict["Фамилия"],
        street=user_dict["Улица"],
        house=user_dict["№ дома"],
        apartment=user_dict["№ Квартиры"],
        accrual_type=int(user_dict["Тип начисления"]),
        previous=float(user_dict["Предыдущее"]),
        current=float(user_dict["Текущее"])
    )


def from_user_to_dict(user):
    """
        Маппер из объекта абонента в словарь для записи

        Parametrs
        ---------
        user: User - объект абонента
    """

    return {
        "№ строки": user.index,
        "Фамилия": user.surname,
        "Улица": user.street,
        "№ дома": user.house,
        "№ Квартиры": user.apartment,
        "Тип начисления": str(user.accrual_type.value),
        "Предыдущее": str(user.previous),
        "Текущее": str(user.current),
        "Начисленно": str(user.accrual)
    }


def from_csv_file_to_users(filename):
    """
        Маппер из csv-файла в список абонентов

        Parametrs
        ---------
        filename: str - путь до файла с абонентами
    """

    if not is_csv_file(filename):
        raise ValueError("File has not CSV type or it's not exists")

    with open(filename, "r", encoding="utf-8-sig") as csv_file:
        csv_headers = csv.reader(
            [csv_file.readline()],
            delimiter=";"
        )

        csv_reader = csv.DictReader(
            csv_file,
            fieldnames=list(csv_headers)[0],
            delimiter=";"
        )

        return list(map(from_dict_to_user, csv_reader))


def from_users_to_csv(users, filename):
    """
        Маппер из списка абонентов в файл

        Paramters
        ---------
        users: [User] - список абонентов
        filename: str - путь до файла, в который нужно сохранить
    """

    with open(filename, "w", encoding="utf-8-sig") as csv_file:
        users_list = list(map(from_user_to_dict, users))

        if not users_list:
            return

        headers = list(users_list[0].keys())
        csv_writer = csv.DictWriter(
            csv_file,
            fieldnames=headers,
            delimiter=";"
        )

        csv_writer.writeheader()
        csv_writer.writerows(users_list)
