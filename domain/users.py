import enum


@enum.unique
class AccuralType(enum.Enum):
    """
        Перечисление способов начисления

        Attributes
        ----------
        BY_STANDARD - оплата по стандарту
        BY_COUNTER - оплата по счётчику
    """

    BY_STANDARD = 1
    BY_COUNTER = 2


class AccuralPrice(enum.Enum):
    """
        Перечисление способов оплаты с их стоимостью

        Attributes
        ----------
        BY_STANDARD - оплата по стандарту
        BY_COUNTER - оплата по счётчику
    """

    BY_STANDARD = 301.26
    BY_COUNTER = 1.52


class User:
    """
        Класс пользователя

        Attributes
        ----------
        index: int - id пользователя в списке
        surname: str - фамилия пользователя
        street: str - улица дома проживания
        house: str - номер дома проживания
        apartment: str - номер квартиры проживания
        accrual_type: AccuralType - тип начисления
        previous: float - предыдущие показания
        current: float - текущие показания
        accrual: float - начисленная сумма
    """

    def __init__(self, **kwargs):
        self.index = kwargs["__index"]
        self.surname = kwargs["surname"]
        self.street = kwargs["street"]
        self.house = kwargs["house"]
        self.apartment = kwargs["apartment"]
        self.accrual_type = AccuralType(kwargs["accrual_type"])
        self.previous = kwargs["previous"]
        self.current = kwargs["current"]
        self.accrual = self.__compute_accrual()

    def __compute_accrual(self):
        """
            Расчёт суммы начисления по тарифу
        """

        if self.accrual_type == AccuralType.BY_STANDARD:
            return AccuralPrice.BY_STANDARD.value

        return AccuralType.BY_COUNTER.value * (self.current - self.previous)
