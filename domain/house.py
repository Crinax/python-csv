class House:
    """
        Класс дома

        Attributes
        ----------
        index: int - id дома в таблице
        street: str - улица дома
        number: str - номер дома
        accrual: float - суммарные начисления дома
    """

    def __init__(self, **kwargs):
        self.index = kwargs["__index"]
        self.street = kwargs["street"]
        self.number = kwargs["number"]
        self.accrual = kwargs["accrual"]
