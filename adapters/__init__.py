from mappers.users import from_csv_file_to_users, from_users_to_csv
from mappers.house import from_user_group_to_house, from_house_to_csv


def compute_accrual(filename, outer_filename):
    """
        Считаем начисление.

        Parameters
        ----------
        filename: str - путь до файла с абонентами
        outer_filename: str - путь, куда будут сохранены расчёты
    """

    user = from_csv_file_to_users(filename)

    return from_users_to_csv(user, outer_filename)


def compute_accrual_sum_by_house(filename, outer_filename):
    """
        Считаем суммарные начисления для каждого дома

        Parameters
        ----------
        filename: str - путь до файла с абонентами
        outer_filename: str - путь, куда будут сохранены расчёты
    """

    users = from_csv_file_to_users(filename)
    houses = from_user_group_to_house(group_users_by_house(users))

    return from_house_to_csv(houses, outer_filename)


def group_users_by_house(users):
    """
        Группировка пользователей по улице и номеру дома одновременно

        Parameters
        ----------
        users: [User] - список пользователей
    """

    result = {}

    for user in users:
        if (user.street, user.house) in result:
            result[(user.street, user.house)].append(user)
        else:
            result[(user.street, user.house)] = [user]

    return result
