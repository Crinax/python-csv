from adapters import compute_accrual, compute_accrual_sum_by_house


def main():
    compute_accrual("абоненты.csv", "Начисления_абоненты.csv")
    compute_accrual_sum_by_house("абоненты.csv", "Начисления_дома.csv")


if __name__ == "__main__":
    main()
